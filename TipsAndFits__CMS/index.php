

<?php
session_start();

if (isset($_SESSION["user"])) {
	
	if(isset($_GET['signout']))
		session_unset();
	
	else
		header('Location:welcome.php');
}

// define variables and set to empty values
// $_SERVER['REQUEST_METHOD'] == "POST" && 

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Login V4</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="vendor/FontAwesome/css/font-awesome.min.css">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="vendor/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="css/util.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
	</head>
	<body>
		<div class="limiter">
			<div class="container-login100" style="background-image: url('img/bg-01.jpg');">
				<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
					<form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" class="login100-form validate-form">
						<span class="login100-form-title p-b-49">
							Tips & Fits
						</span>

						<div class="wrap-input100 validate-input m-b-23" data-validate = "Username is required">
							<span class="label-input100">Username</span>
							<input class="input100" type="text" name="username" placeholder="Type your username">
							<span class="focus-input100" data-symbol="&#xf206;"></span>
						</div>

						<div class="wrap-input100 validate-input" data-validate="Password is required">
							<span class="label-input100">Password</span>
							<input class="input100" type="password" name="pass" placeholder="Type your password">
							<span class="focus-input100" data-symbol="&#xf190;"></span>
						</div>
						
						<div class="text-center p-t-8 p-b-31" style='color:red;'>
						<?php
						if ($_SERVER["REQUEST_METHOD"] == "POST"){

							$username = $userPass = "";
							$username = test_input($_POST["username"]);
							$userPass = test_input($_POST["pass"]);

							if($username=='Admin' && $userPass=='1234'){
								$_SESSION["user"] = "authentic";
								header('Location:welcome.php');
							}
							else{
								echo 'Wrong User-Name or Password';
								// session_unset();		// remove all session variables
								// session_destroy();		// destroy the session
							}
						}

						function test_input($data) {
							$data = trim($data);
							$data = stripslashes($data);
							$data = htmlspecialchars($data);
						  
							return $data;
						}
						?>
						</div>
						
						<div class="container-login100-form-btn">
							<div class="wrap-login100-form-btn">
								<div class="login100-form-bgbtn"></div>
								<button type="submit" name="submitLoginForm" class="login100-form-btn">
									Login
								</button>
							</div>
						</div>

						<div class="txt1 text-center p-t-54 p-b-20"></div>

						<div class="flex-c-m">
							<a href="#" class="login100-social-item bg1">
								<i class="fa fa-facebook"></i>
							</a>

							<a href="#" class="login100-social-item bg2">
								<i class="fa fa-twitter"></i>
							</a>

							<a href="#" class="login100-social-item bg3">
								<i class="fa fa-google"></i>
							</a>
						</div>

						
					</form>
				</div>
			</div>
		</div>
		
		<!--===============================================================================================-->
		<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>