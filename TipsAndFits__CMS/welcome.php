

<?php
session_start();

if (!isset($_SESSION["user"])){
	header('Location:index.php');
}
else{
}
?>

<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Tips & Fits Admin Panel</title>

		
		<!-- Pagination -->
		<link type="text/css" rel="stylesheet" href="css/simplePagination.css"/>
		
		<!-- Bootstrap core CSS -->
		<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>

		<!-- Custom fonts for this template -->
		<link href="vendor/FontAwesome/css/all.min.css" rel="stylesheet"/>
		<link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css"/>

		<!-- Custom styles for this template -->
		<link href="css/landing-page.min.css" rel="stylesheet"/>
		<link href="css/custom.css" rel="stylesheet"/>
	</head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="#home">Tips & Fits</a>
        <a class="btn btn-primary" href="index.php?signout='log me out'">Sign Out</a>
      </div>
    </nav>

    <!-- Masthead -->
    <header class="masthead text-white text-center" id="home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-5">Please Choose Your Options below to Scrutinize with Contents</h1>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
			<a class="btn btn-primary js-scroll-trigger" href="#starting">Find Out More</a>
          </div>
        </div>
      </div>
    </header>

    <!-- Icons Grid -->
    <section class="features-icons bg-light text-center" id="starting">
      <div class="container">
        <div class="row">
          
		  <div class="col-lg-6">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3" data-toggle="modal" data-target="#addTips">
              <div class="features-icons-icon d-flex">
                <i class="icon-screen-desktop m-auto text-primary"></i>
              </div>
              <h3>Add Tips Contents</h3>
              <p class="lead mb-0">Upload Four Contents together</p>
            </div>
          </div>
		  
          <div class="col-lg-6">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3" data-toggle="modal" data-target="#addVideo">
              <div class="features-icons-icon d-flex">
                <i class="icon-control-play m-auto text-primary"></i>
              </div>
              <h3>Add Video & Preview</h3>
              <p class="lead mb-0">Upload Multiple Files Simultaneously</p>
            </div>
          </div>
		  
        </div>
      </div>
    </section>

    <!-- Image Showcases -->
    <section class="showcase">
      <div class="container-fluid p-0">
        
		<div class="row no-gutters">
          <div class="col-lg-6 text-white showcase-img" style="background-image: url('img/bg-showcase-3.jpg');"></div>
          <div class="col-lg-6 my-auto showcase-text">
            <button id="viewContentsButton" class="btn btn-primary" data-toggle="modal" data-target="#viewContents">View Contents</button>
            <p class="lead mb-0">List of all Tips Contents</p>
          </div>
        </div>
		
		<div class="row no-gutters">
          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/bg-showcase-1.jpg');"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <button id="viewFilesButton" class="btn btn-primary" data-toggle="modal" data-target="#viewFiles">Videos & Previews</button>
            <p class="lead mb-0">List of all Videos & Previews</p>
          </div>
        </div>
        
      </div>
    </section>
	
	<section class="text-center" style="margin-top:5rem; text-align:center;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
						<h5>Wanna Download List ?</h5>
						<p class='message'></p>						
						<button type="button" id="exportList" class="btn btn-block btn-primary">Export List</button>	 
					</div>
				</div>
			
			</div>
		</div>
    </section>

    <!-- Footer -->
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="#">About</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Contact</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">Copyright &copy; 2018. All Rights Reserved.</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fab fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fab fa-twitter-square fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fab fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
	
		
		<?php include 'addTips.php';?>
		
		<?php include 'addVideo.php';?>
		
		<?php include 'viewContents.php';?>
		
		<?php include 'viewFiles.php';?>
		
		<?php include 'editingForm.php';?>
		
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded js-scroll-trigger" href="#home">
		  <i class="icon-arrow-up"></i>
		</a>
		
		<!-- Bootstrap core JavaScript and Pagination -->
		<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.simplePagination.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		
		<!-- Plugin JavaScript -->
		<script src="vendor/jquery/jquery.easing.min.js"></script>
		
		<!-- Custom scripts for this template -->
		<script src="vendor/jquery/stylish-portfolio.min.js"></script>
		
		<script src="js/myScript.js"></script>
	</body>
</html>
