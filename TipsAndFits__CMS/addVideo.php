
		<!-- The Add Video Modal -->
		<div class="modal fade" id="addVideo" role="dialog">
			<div class="modal-dialog">
			
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Add Files Modal Header</h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" enctype="multipart/form-data">
							<div class="form-group">
								<label class="control-label" for="date">Date:</label>
								<div>
									<input type="text" class="form-control" name="presentDate" value="<?php echo date("d-m-Y"); ?>" readonly>
								</div>
							</div>
							<div class="form-group"> 
								<label class="control-label" for="category">Category:</label>
								<div>
									<label class="radio-inline"><input type="radio" name="category" value="Animation">Animation</label>
									<label class="radio-inline"><input type="radio" name="category" value="Beauty">Beauty</label>
									<label class="radio-inline"><input type="radio" name="category" value="Fashion">Fashion</label>
									<label class="radio-inline"><input type="radio" name="category" value="Health" checked>Health</label>
									<br/>
									<label class="radio-inline"><input type="radio" name="category" value="Wallpaper">Wallpaper</label>
									<label class="radio-inline"><input type="radio" name="category" value="Download">Download</label>
									<label class="radio-inline"><input type="radio" name="category" value="video">Video</label>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="title">Title:</label>
								<div>
									<input type="text" class="form-control" placeholder="Enter Contents Title" name="title" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="videoPreview">Preview:</label>
								<div>
									<input type="file" class="form-control-file" name="preview[]" multiple required>
								</div>
							</div>
							<div class="form-group" id="videoFile" style="display:none;">
								<label class="control-label" for="videoFile">Video File:</label>
								<div>
									<input type="file" class="form-control-file" name="videoFile[]" multiple required disabled>
								</div>
							</div>
							<div class="form-group"> 
								<label class="control-label" for="status">Status:</label>
								<div>
									<label class="radio-inline"><input type="radio" name="status" value="1" checked>Publishable</label>
									<label class="radio-inline"><input type="radio" name="status" value="0">Unpublishable</label>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="royalty">Royalty:</label>
								<div>
									<input type="text" class="form-control" placeholder="Enter Content Owner" name="royalty">
								</div>
							</div>
							<div class="form-group">        
								<div>
									<button type="submit" class="btn btn-primary">Add</button>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" name="close" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>