	
	$("#addVideo input:radio[name='category']").change(
		function(){
			if ($(this).is(':checked') && $(this).val() != 'video') {
				$("#videoFile input").prop('disabled', true);
				$("#videoFile").hide();
			}else{
				$("#videoFile input").prop('disabled', false);
				$("#videoFile").show();
			}
		}
	);
	
	$("form").submit(function(event){
		
		event.preventDefault();
		var form = $(this)[0];
		// var formId = $(form).attr("id");
		// alert(formId);
		$.ajax({
			type:'POST',
			url:'action.php',
			data : new FormData(form),
			contentType: false,
			processData: false,
			async: false,
			cache: false,
			beforeSend: function(){
				
			},
			success: function(returnData){
				alert(returnData);
				$(form).trigger('reset');
				$("button[name='close']").trigger('click');
			},
			error: function(){
				alert('error handling here');
				$(form).trigger('reset');
				$("button[name='close']").trigger('click');
			}
		});
	});
	
	$("#viewContentsButton").click(function(){
		$.post("action.php", {numberAllContents: "Something"},
			function(result, status){
				
				var tbody = $('#viewContents #viewAllContentsTable tbody');
				
				if($.isNumeric(result) && result != 0){
					var pageList = $('#viewContents #viewContentsPagination');
					$(pageList).pagination({
						items: result,
						itemsOnPage: 10,
						cssStyle: 'light-theme',
						onInit(){
							$.post("action.php", {viewAllContents: 1*10},
								function(results, status){
									if (!jQuery.isEmptyObject(results)){
										
										var obj = JSON.parse(results);
										$(tbody).empty();
										
										for(var i=0; i<obj.length; i++){
											if(typeof(obj[i]) != "undefined" && obj[i] !== null){											
												$(tbody).append("<tr>");
												$(tbody).append("<td>" + obj[i].CONTENTDATE + "</td>");
												$(tbody).append("<td>" + obj[i].CONTENTCATEGORY + "</td>");
												$(tbody).append("<td>" + obj[i].TIPSCONTENT + "</td>");
												$(tbody).append("<td>" + obj[i].STATUS + "</td>");
												$(tbody).append("<td>" + obj[i].CONTENTROYALTY + "</td>");
												$(tbody).append("<td><i class='icon-note' data-type='Text' data-id='"+ obj[i].CONTENTID +"' data-tipsContent='"+ obj[i].TIPSCONTENT +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].CONTENTROYALTY +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='Text' data-id='" + obj[i].CONTENTID + "' style='font-size:18px;color:red'></i></td>");
												$(tbody).append("</tr>");
											}
										}
									}
									else{
										$(tbody).empty();
										$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
									}
								}
							);	
						},
						onPageClick(currentPage,event){
							$.post("action.php", {viewAllContents: currentPage*10},
								function(results, status){
									if (!jQuery.isEmptyObject(results)){
										
										var obj = JSON.parse(results);
										$(tbody).empty();
									
										for(var i=0; i<obj.length; i++){
											if(typeof(obj[i]) != "undefined" && obj[i] !== null){											
												$(tbody).append("<tr>");
												$(tbody).append("<td>" + obj[i].CONTENTDATE + "</td>");
												$(tbody).append("<td>" + obj[i].CONTENTCATEGORY + "</td>");
												$(tbody).append("<td>" + obj[i].TIPSCONTENT + "</td>");
												$(tbody).append("<td>" + obj[i].STATUS + "</td>");
												$(tbody).append("<td>" + obj[i].CONTENTROYALTY + "</td>");
												$(tbody).append("<td><i class='icon-note' data-type='Text' data-id='"+ obj[i].CONTENTID +"' data-tipsContent='"+ obj[i].TIPSCONTENT +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].CONTENTROYALTY +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='Text' data-id='" + obj[i].CONTENTID + "' style='font-size:18px;color:red'></i></td>");
												$(tbody).append("</tr>");
											}
										}
									}
									else{
										$(tbody).empty();
										$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
									}
								}
							);	
						}
					});
				}
				else{
					$(tbody).empty();
					$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
				}
				
			}
		);
	});
	
	$('#viewContents #viewFilteredContentsTable input[type=radio][name=specified]').change(
		function(){
			var expectedCategoryContents = $(this).val();
			$.post("action.php", {numberExpectedContents: $(this).val()},
				function(result, status){
					
					var tbody = $('#viewContents #viewAllContentsTable tbody');
					
					if($.isNumeric(result) && result != 0){
						var pageList = $('#viewContents #viewContentsPagination');
						$(pageList).pagination({
							items: result,
							itemsOnPage: 10,
							cssStyle: 'light-theme',
							onInit(){
								$.post("action.php",
									{
									expectedCategoryContents : expectedCategoryContents, 
									viewFilteredContents : 1*10
									},
									function(results, status){
										if (!jQuery.isEmptyObject(results)){
											
											var obj = JSON.parse(results);
											$(tbody).empty();
											
											for(var i=0; i<obj.length; i++){
												if(typeof(obj[i]) != "undefined" && obj[i] !== null){											
													$(tbody).append("<tr>");
													$(tbody).append("<td>" + obj[i].CONTENTDATE + "</td>");
													$(tbody).append("<td>" + obj[i].CONTENTCATEGORY + "</td>");
													$(tbody).append("<td>" + obj[i].TIPSCONTENT + "</td>");
													$(tbody).append("<td>" + obj[i].STATUS + "</td>");
													$(tbody).append("<td>" + obj[i].CONTENTROYALTY + "</td>");
													$(tbody).append("<td><i class='icon-note' data-type='Text' data-id='"+ obj[i].CONTENTID +"' data-tipsContent='"+ obj[i].TIPSCONTENT +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].CONTENTROYALTY +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='Text' data-id='" + obj[i].CONTENTID + "' style='font-size:18px;color:red'></i></td>");
													$(tbody).append("</tr>");
												}
											}
										}
										else{
											$(tbody).empty();
											$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
										}
									}
								);	
							},
							onPageClick(currentPage,event){
								$.post("action.php",
									{
									expectedCategoryContents: expectedCategoryContents, 
									viewFilteredContents: currentPage*10
									},
									function(results, status){
										if (!jQuery.isEmptyObject(results)){
											
											var obj = JSON.parse(results);
											$(tbody).empty();
											
											for(var i=0; i<obj.length; i++){
												if(typeof(obj[i]) != "undefined" && obj[i] !== null){											
													$(tbody).append("<tr>");
													$(tbody).append("<td>" + obj[i].CONTENTDATE + "</td>");
													$(tbody).append("<td>" + obj[i].CONTENTCATEGORY + "</td>");
													$(tbody).append("<td>" + obj[i].TIPSCONTENT + "</td>");
													$(tbody).append("<td>" + obj[i].STATUS + "</td>");
													$(tbody).append("<td>" + obj[i].CONTENTROYALTY + "</td>");
													$(tbody).append("<td><i class='icon-note' data-type='Text' data-id='"+ obj[i].CONTENTID +"' data-tipsContent='"+ obj[i].TIPSCONTENT +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].CONTENTROYALTY +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='Text' data-id='" + obj[i].CONTENTID + "' style='font-size:18px;color:red'></i></td>");
													$(tbody).append("</tr>");
												}
											}
										}
										else{
											$(tbody).empty();
											$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
										}
									}
								);	
							}
						});
					}
					else{
						$(tbody).empty();
						$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
					}
				}
			);
		}
	);
	
	$("#viewFilesButton").click(function(){
		$.post("action.php", {numberAllFiles: "Something"},
			function(result, status){
				
				var tbody = $('#viewFiles #viewAllFilesTable tbody');
				
				if($.isNumeric(result) && result != 0){
					var pageList = $('#viewFiles #viewFilesPagination');
					$(pageList).pagination({
						items: result,
						itemsOnPage: 10,
						cssStyle: 'light-theme',
						onInit(){
							$.post("action.php", {viewAllFiles: 1*10},
								function(results, status){
									if (!jQuery.isEmptyObject(results)){
									
										var obj = JSON.parse(results);
										$(tbody).empty();
										
										for(var i=0; i<obj.length; i++){
											if(typeof(obj[i]) != "undefined" && obj[i] !== null){	
												$(tbody).append("<tr class='success'>");
												$(tbody).append("<td>" + obj[i].FILECREATED + "</td>");
												$(tbody).append("<td>" + obj[i].FILETITLE + "</td>");
												$(tbody).append("<td>" + obj[i].FILECATEGORY + "</td>");
												$(tbody).append("<td><img src='" + obj[i].PREVIEWPATH + "' alt='NoImage' width='42' height='42'></td>");
												$(tbody).append("<td>" + obj[i].STATUS + "</td>");
												$(tbody).append("<td>" + obj[i].FILEROYALTY + "</td>");
												$(tbody).append("<td><i class='icon-note' data-type='File' data-id='"+ obj[i].FILEID +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].FILEROYALTY +"' data-title='"+ obj[i].FILETITLE +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='File' data-id='" + obj[i].FILEID + "' style='font-size:18px;color:red'></i></td>");
												$(tbody).append("</tr>");
											}
										}
									}
									else{
										$(tbody).empty();
										$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
									}
								}
							);	
						},
						onPageClick(currentPage,event){
							$.post("action.php", {viewAllFiles: currentPage*10},
								function(results, status){
									if (!jQuery.isEmptyObject(results)){
										
										var obj = JSON.parse(results);
										$(tbody).empty();
										
										for(var i=0; i<obj.length; i++){
											if(typeof(obj[i]) != "undefined" && obj[i] !== null){	
												$(tbody).append("<tr class='success'>");
												$(tbody).append("<td>" + obj[i].FILECREATED + "</td>");
												$(tbody).append("<td>" + obj[i].FILETITLE + "</td>");
												$(tbody).append("<td>" + obj[i].FILECATEGORY + "</td>");
												$(tbody).append("<td><img src='" + obj[i].PREVIEWPATH + "' alt='NoImage' width='42' height='42'></td>");
												$(tbody).append("<td>" + obj[i].STATUS + "</td>");
												$(tbody).append("<td>" + obj[i].FILEROYALTY + "</td>");
												$(tbody).append("<td><i class='icon-note' data-type='File' data-id='"+ obj[i].FILEID +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].FILEROYALTY +"' data-title='"+ obj[i].FILETITLE +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='File' data-id='" + obj[i].FILEID + "' style='font-size:18px;color:red'></i></td>");
												$(tbody).append("</tr>");
											}
										}
									}
									else{
										$(tbody).empty();
										$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
									}
								}
							);	
						}
					});
				}
				else{
					$(tbody).empty();
					$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");	
				}
			}
		);
	});
	
	$('#viewFiles #viewFilteredFilesTable input[type=radio][name=specified]').change(
		function(){
			// alert($(this).val());
			var expectedCategoryFiles = $(this).val();
			$.post("action.php", {numberExpectedFiles: $(this).val()},
				function(result, status){
					
					var tbody = $('#viewFiles #viewAllFilesTable tbody');
					
					if($.isNumeric(result) && result != 0){
						var pageList = $('#viewFiles #viewFilesPagination');
						$(pageList).pagination({
							items: result,
							itemsOnPage: 10,
							cssStyle: 'light-theme',
							onInit(){
								$.post("action.php",
									{
									expectedCategoryFiles : expectedCategoryFiles, 
									viewFilteredFiles : 1*10
									},
									function(results, status){
										if (!jQuery.isEmptyObject(results)){
											
											var obj = JSON.parse(results);
											$(tbody).empty();
											
											for(var i=0; i<obj.length; i++){
												if(typeof(obj[i]) != "undefined" && obj[i] !== null){	
													$(tbody).append("<tr class='success'>");
													$(tbody).append("<td>" + obj[i].FILECREATED + "</td>");
													$(tbody).append("<td>" + obj[i].FILETITLE + "</td>");
													$(tbody).append("<td>" + obj[i].FILECATEGORY + "</td>");
													$(tbody).append("<td><img src='" + obj[i].PREVIEWPATH + "' alt='NoImage' width='42' height='42'></td>");
													$(tbody).append("<td>" + obj[i].STATUS + "</td>");
													$(tbody).append("<td>" + obj[i].FILEROYALTY + "</td>");
													$(tbody).append("<td><i class='icon-note' data-type='File' data-id='"+ obj[i].FILEID +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].FILEROYALTY +"' data-title='"+ obj[i].FILETITLE +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='File' data-id='" + obj[i].FILEID + "' style='font-size:18px;color:red'></i></td>");
													$(tbody).append("</tr>");
												}
											}
										}
										else{
											$(tbody).empty();
											$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
										}
									}
								);	
							},
							onPageClick(currentPage,event){
								$.post("action.php",
									{
									expectedCategoryFiles : expectedCategoryFiles, 
									viewFilteredFiles : currentPage*10
									},
									function(results, status){
										if (!jQuery.isEmptyObject(results)){
											var obj = JSON.parse(results);
											$(tbody).empty();
											
											for(var i=0; i<obj.length; i++){
												if(typeof(obj[i]) != "undefined" && obj[i] !== null){	
													$(tbody).append("<tr class='success'>");
													$(tbody).append("<td>" + obj[i].FILECREATED + "</td>");
													$(tbody).append("<td>" + obj[i].FILETITLE + "</td>");
													$(tbody).append("<td>" + obj[i].FILECATEGORY + "</td>");
													$(tbody).append("<td><img src='" + obj[i].PREVIEWPATH + "' alt='NoImage' width='42' height='42'></td>");
													$(tbody).append("<td>" + obj[i].STATUS + "</td>");
													$(tbody).append("<td>" + obj[i].FILEROYALTY + "</td>");
													$(tbody).append("<td><i class='icon-note' data-type='File' data-id='"+ obj[i].FILEID +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].FILEROYALTY +"' data-title='"+ obj[i].FILETITLE +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='File' data-id='" + obj[i].FILEID + "' style='font-size:18px;color:red'></i></td>");
													$(tbody).append("</tr>");
												}
											}
										
										}
										else{
											$(tbody).empty();
											$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
										}
									}
								);	
							}
						});
					}
					else{
						$(tbody).empty();
						$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
					}
				}
			);
		}
	);
	
	
	
	
	
	$(document).ready(function(){
		$(document).on("click","i.icon-trash",function(){
			if(confirm("Confirmation to Delete")){
				$.post("action.php", 
					{type: $(this).data('type'), deleteId: $(this).attr('data-id')},
					function(results, status){
						console.log(results);
						$('<div>'+results+'!</div>').insertBefore('.message').fadeOut(4000,'swing');
					}
				);
			}
		});
	});
	
	$(document).ready(function(){
		$(document).on("click","i.icon-note",function(){

			if(confirm("Confirmation of Edition")){
				$("#editItems").modal();
				$("#editItems input[name='type']").val($(this).data('type'));
				$("#editItems input[name='editId']").val($(this).attr('data-id'));
				$('#editItems input:radio[name=status]').filter("[value="+$(this).attr('data-status')+"]").prop('checked', true);
				$("#editItems input[name='royalty']").val($(this).data('royalty'));
					
				if($(this).data('type')=='Text'){
					// $("#editItems #title input[name='title']").prop('disabled', true);
					$("#editItems #title").hide();
					$("#editItems #tipsContent textarea").prop('disabled', false);
					$("#editItems #tipsContent").show();
					$("#editItems #tipsContent textarea").val($(this).attr('data-tipsContent'));
				}
				else{
					$("#editItems #title").show();
					$("#editItems #title input[name='title']").val($(this).data('title'));
					$("#editItems #tipsContent textarea").prop('disabled', true);
					$("#editItems #tipsContent").hide();					
				}
			}
		});
	});
	
	$(document).ready(function(){
		$('#editItems').on('hidden.bs.modal', function () {
			$("button[name='close']").trigger('click');
		})
	});
	
	$("#exportList").click(function(){
		if(confirm("Export Contents List ?")){
			$.post("action.php", {exportList: "Something"},
				function(results, status){
					if (!jQuery.isEmptyObject(results)){
						$('.message').html('Exported').fadeOut(4000,'swing');
					}
					else{
						console.log("Result is blank");
					}
				}
			);
		}
	});
	
	/*
	$('#viewContents #viewFilteredContentsTable input[type=radio][name=specified]').change(
		function(){
			
			
			var indexFilteredContents = new Array();
			for(var i=0; i<viewAllContents.length; i++) {
				if(viewAllContents[i].CONTENTCATEGORY == $(this).val()){
					indexFilteredContents.push(i);
				}
			}
			
			var pageList = $('#viewContents #viewContentsPagination');
			$(pageList).pagination({
				items: indexFilteredContents.length,
				itemsOnPage: 10,
				cssStyle: 'light-theme',
				onInit(){
					$(tbody).empty();
					for(var i=0; i<10; i++){
						if(typeof(viewAllContents[indexFilteredContents[i]]) != "undefined" && viewAllContents[indexFilteredContents[i]] !== null){
							$(tbody).append("<tr>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].CONTENTDATE + "</td>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].CONTENTCATEGORY + "</td>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].TIPSCONTENT + "</td>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].STATUS + "</td>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].CONTENTROYALTY + "</td>");
							$(tbody).append("<td><i class='icon-note' data-type='Text' data-id='"+ viewAllContents[indexFilteredContents[i]].CONTENTID +"' data-tipsContent='"+ viewAllContents[indexFilteredContents[i]].TIPSCONTENT +"' data-status='"+ viewAllContents[indexFilteredContents[i]].STATUS +"' data-royalty='"+ viewAllContents[indexFilteredContents[i]].CONTENTROYALTY +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='Text' data-id='" + viewAllContents[indexFilteredContents[i]].CONTENTID + "' style='font-size:18px;color:red'></i></td>");
							$(tbody).append("</tr>");
						}
					}
					
				},
				onPageClick(currentPage,event){
					$(tbody).empty();
					var startingLimit = (currentPage-1)*10;
					for(var i=startingLimit; i<startingLimit+10; i++){
						if(typeof(viewAllContents[indexFilteredContents[i]]) != "undefined" && viewAllContents[indexFilteredContents[i]] !== null){										
							$(tbody).append("<tr>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].CONTENTDATE + "</td>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].CONTENTCATEGORY + "</td>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].TIPSCONTENT + "</td>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].STATUS + "</td>");
							$(tbody).append("<td>" + viewAllContents[indexFilteredContents[i]].CONTENTROYALTY + "</td>");
							$(tbody).append("<td><i class='icon-note' data-type='Text' data-id='"+ viewAllContents[indexFilteredContents[i]].CONTENTID +"' data-tipsContent='"+ viewAllContents[indexFilteredContents[i]].TIPSCONTENT +"' data-status='"+ viewAllContents[indexFilteredContents[i]].STATUS +"' data-royalty='"+ viewAllContents[indexFilteredContents[i]].CONTENTROYALTY +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='Text' data-id='" + viewAllContents[indexFilteredContents[i]].CONTENTID + "' style='font-size:18px;color:red'></i></td>");
							$(tbody).append("</tr>");
						}
					}
				}
			});	
		}
	);
	
	var viewAllFiles;
	$("#viewFilesButton").click(function(){
		$.post("action.php", {viewAllFiles: "Something"},
			function(results, status){
				var obj = jQuery.parseJSON(results);
				viewAllFiles = obj;
				var tbody = $('#viewFiles #viewAllFilesTable tbody');
				
				if (!jQuery.isEmptyObject(results)){
					var pageList = $('#viewFiles #viewFilesPagination');
					$(pageList).pagination({
						items: obj.length,
						itemsOnPage: 10,
						cssStyle: 'light-theme',
						onInit(){
							$(tbody).empty();
							for(var i=0; i<10; i++){
								if(typeof(obj[i]) != "undefined" && obj[i] !== null){	
									$(tbody).append("<tr class='success'>");
									$(tbody).append("<td>" + obj[i].FILECREATED + "</td>");
									$(tbody).append("<td>" + obj[i].FILETITLE + "</td>");
									$(tbody).append("<td>" + obj[i].FILECATEGORY + "</td>");
									$(tbody).append("<td><img src='" + obj[i].PREVIEWPATH + "' alt='NoImage' width='42' height='42'></td>");
									$(tbody).append("<td>" + obj[i].STATUS + "</td>");
									$(tbody).append("<td>" + obj[i].FILEROYALTY + "</td>");
									$(tbody).append("<td><i class='icon-note' data-type='File' data-id='"+ obj[i].FILEID +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].FILEROYALTY +"' data-title='"+ obj[i].FILETITLE +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='File' data-id='" + obj[i].FILEID + "' style='font-size:18px;color:red'></i></td>");
									$(tbody).append("</tr>");
								}
							}
						},
						onPageClick(currentPage,event){
							$(tbody).empty();
							var startingLimit = (currentPage-1)*10;
							for(var i=startingLimit; i<startingLimit+10; i++){
								if(typeof(obj[i]) != "undefined" && obj[i] !== null){											
									$(tbody).append("<tr class='success'>");
									$(tbody).append("<td>" + obj[i].FILECREATED + "</td>");
									$(tbody).append("<td>" + obj[i].FILETITLE + "</td>");
									$(tbody).append("<td>" + obj[i].FILECATEGORY + "</td>");
									$(tbody).append("<td><img src='" + obj[i].PREVIEWPATH + "' alt='NoImage' width='42' height='42'></td>");
									$(tbody).append("<td>" + obj[i].STATUS + "</td>");
									$(tbody).append("<td>" + obj[i].FILEROYALTY + "</td>");
									$(tbody).append("<td><i class='icon-note' data-type='File' data-id='"+ obj[i].FILEID +"' data-status='"+ obj[i].STATUS +"' data-royalty='"+ obj[i].FILEROYALTY +"' data-title='"+ obj[i].FILETITLE +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='File' data-id='" + obj[i].FILEID + "' style='font-size:18px;color:red'></i></td>");
									$(tbody).append("</tr>");
								}
							}
						}
					});
				
				}
				else{
					$(tbody).empty();
					$(tbody).append("<div class='col-sm-12 text-danger text-center'>No Data</div>");
				}
			}
		);
	});
	
	$('#viewFiles #viewFilteredFilesTable input[type=radio][name=specified]').change(
		function(){
			var tbody = $('#viewFiles #viewAllFilesTable tbody');
			
			var indexFilteredFiles = new Array();
			for(var i=0; i<viewAllFiles.length; i++) {
				if(viewAllFiles[i].FILECATEGORY == $(this).val()){
					indexFilteredFiles.push(i);
				}
			}
			
			var pageList = $('#viewFiles #viewFilesPagination');
			$(pageList).pagination({
				items: indexFilteredFiles.length,
				itemsOnPage: 10,
				cssStyle: 'light-theme',
				onInit(){
					$(tbody).empty();
					for(var i=0; i<10; i++){
						if(typeof(viewAllFiles[indexFilteredFiles[i]]) != "undefined" && viewAllFiles[indexFilteredFiles[i]] !== null){	
							$(tbody).append("<tr class='danger'>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].FILECREATED + "</td>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].FILETITLE + "</td>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].FILECATEGORY + "</td>");
							$(tbody).append("<td><img src='" + viewAllFiles[indexFilteredFiles[i]].PREVIEWPATH + "' alt='NoImage' width='42' height='42'></td>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].STATUS + "</td>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].FILEROYALTY + "</td>");
							$(tbody).append("<td><i class='icon-note' data-type='File' data-id='"+ viewAllFiles[indexFilteredFiles[i]].FILEID +"' data-status='"+ viewAllFiles[indexFilteredFiles[i]].STATUS +"' data-royalty='"+ viewAllFiles[indexFilteredFiles[i]].FILEROYALTY +"' data-title='"+ viewAllFiles[indexFilteredFiles[i]].FILETITLE +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='File' data-id='" + viewAllFiles[indexFilteredFiles[i]].FILEID + "' style='font-size:18px;color:red'></i></td>");
							$(tbody).append("</tr>");
						}
					}
					
				},
				onPageClick(currentPage,event){
					$(tbody).empty();
					var startingLimit = (currentPage-1)*10;
					for(var i=startingLimit; i<startingLimit+10; i++){
						if(typeof(viewAllFiles[indexFilteredFiles[i]]) != "undefined" && viewAllFiles[indexFilteredFiles[i]] !== null){											
							$(tbody).append("<tr class='danger'>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].FILECREATED + "</td>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].FILETITLE + "</td>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].FILECATEGORY + "</td>");
							$(tbody).append("<td><img src='" + viewAllFiles[indexFilteredFiles[i]].PREVIEWPATH + "' alt='NoImage' width='42' height='42'></td>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].STATUS + "</td>");
							$(tbody).append("<td>" + viewAllFiles[indexFilteredFiles[i]].FILEROYALTY + "</td>");
							$(tbody).append("<td><i class='icon-note' data-type='File' data-id='"+ viewAllFiles[indexFilteredFiles[i]].FILEID +"' data-status='"+ viewAllFiles[indexFilteredFiles[i]].STATUS +"' data-royalty='"+ viewAllFiles[indexFilteredFiles[i]].FILEROYALTY +"' data-title='"+ viewAllFiles[indexFilteredFiles[i]].FILETITLE +"' style='font-size:18px;color:red'></i><i class='icon-trash' data-type='File' data-id='" + viewAllFiles[indexFilteredFiles[i]].FILEID + "' style='font-size:18px;color:red'></i></td>");
							$(tbody).append("</tr>");
						}
					}
				}
			});	
		}
	);
	*/
