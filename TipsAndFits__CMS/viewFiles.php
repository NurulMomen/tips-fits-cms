<!-- The View Files Modal -->
		<div class="modal fade" id="viewFiles" role="dialog">
			<div class="modal-dialog" style='max-width:100%'>
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">View Files Modal Header</h4>
					</div>
					<table class="table" id="viewFilteredFilesTable">
						<thead>
							<tr style="color:blue;">
								<th><span> Filter :</span></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Animation"> Animation</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Beauty"> Beauty</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Fashion"> Fashion</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Health"> Health</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Wallpaper"> Wallpaper</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Download"> Download</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="video"> Video</label></th>
							</tr>
						</thead>
					</table>
					<div class="modal-body text-center">
						<p class='message'></p>
						<table class="table table-striped table-hover table-bordered text-center" id="viewAllFilesTable">
						    <thead>
								<tr>
									<th>File Date</th>
									<th>Title</th>
									<th>File Category</th>
									<th>Previews</th>
									<th>Status</th>
									<th>File Royalty</th>
									<th>Scrutinize</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<ul class='clearfix text-center' id="viewFilesPagination"></ul>
					</div>
					<div class="modal-footer">
						<button type="button" name='close' class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>