	<?php 
		$conn=oci_connect("system","Oracle_1","//localhost/orcl");
	?>
	
	<?php
	
	if (isset($_POST["category"],$_POST['tipsContent'])) {
		
		$category = $_POST["category"];
		$royalty = $_POST["royalty"];
		$status = $_POST["status"];
		$tipsGiven = count(array_filter($_POST["tipsContent"]));
		
		$queryStatus=1; 
		for( $i=0 ; $i < $tipsGiven; $i++ ) {
			$tipsContent[$i] = $_POST["tipsContent"][$i];
			
			$query = "INSERT INTO CONTENTDETAILS(CONTENTCATEGORY, TIPSCONTENT, CONTENTROYALTY, STATUS)
			VALUES('$category', '$tipsContent[$i]', '$royalty', '$status')";
			$parsed_Query = oci_parse($conn,$query);
			$success = oci_execute($parsed_Query);
			
			if($success){
				$queryStatus *= 1;
			}
			else {
				$queryStatus *= 0;
			}
		}
		
		if($queryStatus){
			echo "All Contents are Added Successfully ";
		}
		else {
			echo "Content Addition is Failed";
		}
	}
	 
	else if(isset($_FILES['preview']['name'])){
		
		$category = $_POST["category"];
		$royalty = $_POST["royalty"];
		$status = $_POST["status"];
		
		$preview_dir = './repository/previews/'.$category.'/';
		
		if (!file_exists($preview_dir)) {
			mkdir($preview_dir,0777,true);
		}
		
		$charachtersToRemove = array('"', ' ', '|', '&', '-', '/');
		
		if($category=='video'){
			$videoFile_dir = './repository/'.$category.'/';
			if (!file_exists($videoFile_dir)) {
				mkdir($videoFile_dir,0777,true);
			}
			$smallerNumber = min(count($_FILES['preview']['name']),count($_FILES['videoFile']['name']));			
		}else{
			$smallerNumber = count($_FILES['preview']['name']);
		}
		
		$queryStatus = 1;
		for( $i=0 ; $i < $smallerNumber; $i++){	
			
			$title = $_POST["title"];
			$title = $title.'-'.($i+1);
			
			$contentPreviewName[$i] = substr($_FILES['preview']['name'][$i],-10);
			$contentExtension[$i] =  pathinfo($contentPreviewName[$i],PATHINFO_EXTENSION);
			$contentNewName[$i] =  str_replace($charachtersToRemove, '_', $contentPreviewName[$i]);			
			$contentPreview_path[$i] = $preview_dir.$contentNewName[$i];	
			
			if (!file_exists($contentPreview_path[$i])){
				move_uploaded_file($_FILES['preview']['tmp_name'][$i], $contentPreview_path[$i]);
			}
		
			if($category=='video'){
				$contentVideoName[$i] = substr($_FILES['videoFile']['name'][$i],-10);
				$contentExtension[$i] =  pathinfo($contentVideoName[$i],PATHINFO_EXTENSION);
				$contentNewName[$i] =  str_replace($charachtersToRemove, '_', $contentVideoName[$i]);			
				$contentVideo_path[$i] = $videoFile_dir.$contentNewName[$i];	
				
				if (!file_exists($contentVideo_path[$i])){
					move_uploaded_file($_FILES['videoFile']['tmp_name'][$i], $contentVideo_path[$i]);
				}	
			}else{
				$contentVideo_path[$i] = 'NA';
			}
			
			$query = "INSERT INTO CONTENTFILES(FILECATEGORY, PREVIEWPATH, FILEROYALTY, VIDEOPATH, STATUS, FILETITLE)
			VALUES('$category', '$contentPreview_path[$i]', '$royalty', '$contentVideo_path[$i]', '$status', '$title')";
			$parsed_Query = oci_parse($conn,$query);
			$success = oci_execute($parsed_Query);
			
			if($success){
				$queryStatus *= 1;
			}
			else {
				$queryStatus *= 0;
			}
		}
		
		if($queryStatus){
			echo "All Files are Added Successfully ";
		}
		else {
			echo "File Addition is Failed";
		}
	}
	
	else if (isset($_POST["numberAllContents"])){
		$query = "SELECT COUNT(CONTENTID) FROM CONTENTDETAILS";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		if($success){
			$numrows = oci_fetch_array($parsed_Query, OCI_BOTH);
			echo $numrows[0];	
		}
		else{
			echo 'Failed';	
		}
	}
	
	else if(isset($_POST['viewAllContents'])){
		$currentContents = $_POST['viewAllContents'];
		$previousContents = $_POST['viewAllContents']-10;
		
		$query = "SELECT * FROM (SELECT CONTENTDETAILS.*, ROWNUM SERIAL FROM CONTENTDETAILS WHERE ROWNUM <= '$currentContents' ORDER BY CONTENTID DESC) WHERE SERIAL > '$previousContents'";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		
		// header("Content-Type: application/json; charset=UTF-8");
		if($success){		
			while($row = oci_fetch_assoc($parsed_Query))
			{
				$results[] = $row;
			}
		}
		else{
			return false;
		}
		// echo json_encode($results, JSON_FORCE_OBJECT);
		if(isset($results))
			echo json_encode($results);
	}
	
	else if (isset($_POST["numberExpectedContents"])){
		$expectedCategory = $_POST["numberExpectedContents"];
		
		$query = "SELECT COUNT(CONTENTID) FROM CONTENTDETAILS WHERE CONTENTCATEGORY = '$expectedCategory'";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		if($success){
			$numrows = oci_fetch_array($parsed_Query, OCI_BOTH);
			echo $numrows[0];	
		}
		else{
			echo 'Failed';	
		}
	}
	
	else if(isset($_POST['viewFilteredContents'])){
		$expectedCategory = $_POST["expectedCategoryContents"];
		$currentContents = $_POST['viewFilteredContents'];
		$previousContents = $_POST['viewFilteredContents']-10;
		
		$query = "SELECT * FROM (SELECT CONTENTDETAILS.*, ROWNUM SERIAL FROM CONTENTDETAILS WHERE CONTENTCATEGORY = '$expectedCategory' AND ROWNUM <= '$currentContents' ORDER BY CONTENTID DESC) WHERE SERIAL > '$previousContents'";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		
		// header("Content-Type: application/json; charset=UTF-8");
		if($success){		
			while($row = oci_fetch_assoc($parsed_Query))
			{
				$results[] = $row;
			}
		}
		else{
			return false;
		}
		// echo json_encode($results, JSON_FORCE_OBJECT);
		if(isset($results))
			echo json_encode($results);
	}
	
	else if (isset($_POST["numberAllFiles"])){
		$query = "SELECT COUNT(FILEID) FROM CONTENTFILES";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		if($success){
			$numrows = oci_fetch_array($parsed_Query, OCI_BOTH);
			echo $numrows[0];	
		}
		else{
			echo 'Failed';	
		}
	}
	
	else if(isset($_POST['viewAllFiles'])){
		$currentFiles = $_POST['viewAllFiles'];
		$previousFiles = $_POST['viewAllFiles']-10;
		
		$query = "SELECT * FROM (SELECT CONTENTFILES.*, ROWNUM SERIAL FROM CONTENTFILES WHERE ROWNUM <= '$currentFiles' ORDER BY FILEID DESC) WHERE SERIAL > '$previousFiles'";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		
		// header("Content-Type: application/json; charset=UTF-8");
		if($success){		
			while($row = oci_fetch_assoc($parsed_Query))
			{
				$results[] = $row;
			}
		}
		else{
			return false;
		}
		// echo json_encode($results, JSON_FORCE_OBJECT);
		if(isset($results))
			echo json_encode($results);
	}
	
	
	else if (isset($_POST["numberExpectedFiles"])){
		$expectedCategory = $_POST["numberExpectedFiles"];
		
		$query = "SELECT COUNT(FILEID) FROM CONTENTFILES WHERE FILECATEGORY = '$expectedCategory'";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		if($success){
			$numrows = oci_fetch_array($parsed_Query, OCI_BOTH);
			echo $numrows[0];	
		}
		else{
			echo 'Failed';	
		}
	}
	
	else if(isset($_POST['viewFilteredFiles'])){
		$expectedCategory = $_POST["expectedCategoryFiles"];
		$currentFiles = $_POST['viewFilteredFiles'];
		$previousFiles = $_POST['viewFilteredFiles']-10;
		
		$query = "SELECT * FROM (SELECT CONTENTFILES.*, ROWNUM SERIAL FROM CONTENTFILES WHERE FILECATEGORY = '$expectedCategory' AND ROWNUM <= '$currentFiles' ORDER BY FILEID DESC) WHERE SERIAL > '$previousFiles'";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		
		// header("Content-Type: application/json; charset=UTF-8");
		if($success){		
			while($row = oci_fetch_assoc($parsed_Query))
			{
				$results[] = $row;
			}
		}
		else{
			
		}
		// echo json_encode($results, JSON_FORCE_OBJECT);
		if(isset($results))
			echo json_encode($results);
	}
	
	else if (isset($_POST["deleteId"])){
		
		$deleteId=$_POST["deleteId"];
		$type=$_POST["type"];
		
		if($type=='File')
			$query = "DELETE FROM CONTENTFILES WHERE FILEID = '$deleteId'";
		else
			$query = "DELETE FROM CONTENTDETAILS WHERE CONTENTID = '$deleteId'";
	
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		
		if($success){		
			echo 'Successfully Deleted';
		}
		else{
			echo 'Deletion Failed';
		}
	}
	
	else if (isset($_POST['editId'])){
		
		$type=$_POST['type'];
		$editId=$_POST['editId'];
		$status=$_POST['status'];
		$royalty=$_POST['royalty'];
		
		if($type=='Text'){
			$tipsContent=$_POST['tipsContent'];
		
			$query = "UPDATE CONTENTDETAILS SET TIPSCONTENT = '$tipsContent', STATUS = '$status', 
			CONTENTROYALTY = '$royalty' WHERE CONTENTID = '$editId'";
		}
		else{
			$title=$_POST['title'];
			
			$query = "UPDATE CONTENTFILES SET STATUS = '$status', 
			FILEROYALTY = '$royalty', FILETITLE = '$title' WHERE FILEID = '$editId'";
		}
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);
		
		if($success){		
			echo 'Successfully Updated';
		}
		else{
			echo 'Edition Failed';
		}
	}
	
	else if(isset($_POST["exportList"])){

		$spreadshit_dir = 'spreadsheet/';
		
		if (!file_exists($spreadshit_dir)) {
			mkdir($spreadshit_dir,0777,true);
		}
	
		$filename = date('d-m-Y').".csv"; 
		$output = fopen($spreadshit_dir.$filename, 'w+'); 
		
		fputcsv($output, array('Date', 'Category', 'Content', 'Status', 'Royalty'));
		
		$query = "SELECT CONTENTDATE, CONTENTCATEGORY, TIPSCONTENT, STATUS, CONTENTROYALTY FROM CONTENTDETAILS ORDER BY CONTENTCATEGORY ASC, CONTENTDATE DESC";
		$parsed_Query = oci_parse($conn,$query);
		$success = oci_execute($parsed_Query);

		while ($result_Data = oci_fetch_assoc($parsed_Query)){
			fputcsv($output, $result_Data);
			echo '<br>';
		}
		
		header("Content-Type: application/csv");
		header("Content-disposition: attachment; filename={$filename}");
		
		return 'Exported';
		// echo $filename;
	}
	
	?>
	
	
	<?php 
		oci_close($conn);
	?>