<!-- The View Contents Modal -->
		<div class="modal fade" id="viewContents" role="dialog">
			<div class="modal-dialog" style='max-width:100%'>
			
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">View Contents Modal Header</h4>
					</div>
					<table class="table" id="viewFilteredContentsTable">
						<thead>
							<tr style="color:blue;">
								<th><span> Filter :</span></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Animation"> Animation</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Beauty"> Beauty</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Fashion"> Fashion</label></th>
								<th><label class="radio-inline"><input type="radio" name="specified" value="Health"> Health</label></th>
							</tr>
						</thead>
					</table>
					<div class="modal-body text-center">
						<p class='message'></p>
						<table class="table table-striped table-hover table-bordered text-center" id="viewAllContentsTable">
						    <thead>
								<tr>
									<th>Content Date</th>
									<th>Content Category</th>
									<th>Content</th>
									<th>Status</th>
									<th>Content Royalty</th>
									<th>Scrutinize</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div class='clearfix text-center'>
							<ul class='' id="viewContentsPagination"></ul>						
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" name='close' class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>