
		<!-- Editing Form -->
		<div class="modal fade" id="editItems" role="dialog" style="background-color: #f8f9fa">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Editing Form</h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" enctype="multipart/form-data">
							<div class="form-group">
								<label class="control-label" for="date">Date:</label>
								<div>
									<input type="text" class="form-control" name="presentDate" value="<?php echo date("d-m-Y"); ?>" readonly>
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<div>
									<input type="text" class="form-control" name="type" readonly>
								</div>
							</div>
							<div class="form-group" style="display:none;">
								<div>
									<input type="text" class="form-control" name="editId" readonly>
								</div>
							</div>
							<div class="form-group" id="title">
								<label class="control-label" for="title">Title:</label>
								<div>
									<input type="text" class="form-control" name="title">
								</div>
							</div>
							<div class="form-group" id="tipsContent">
								<label class="control-label" for="description">Description:</label>
								<div class="col-sm-12">
									<textarea class="form-control" rows="3" placeholder="Contents of Tip" name="tipsContent" required></textarea>
								</div>
							</div>
							<div class="form-group"> 
								<label class="control-label" for="status">Status:</label>
								<div>
									<label class="radio-inline"><input type="radio" name="status" value="1">Publishable</label>
									<label class="radio-inline"><input type="radio" name="status" value="0">Unpublishable</label>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="royalty">Royalty:</label>
								<div>
									<input type="text" class="form-control" placeholder="Enter Content Owner" name="royalty">
								</div>
							</div>
							<div class="form-group">        
								<div>
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" name="close" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>